# 145 - Terminal Clock

145 is an ASCII digital clock written in Rust.

## Build

Build the program with

```sh
cargo build
```

Run with 

```sh
cargo run
```