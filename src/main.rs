extern crate chrono;
extern crate termion;
extern crate ctrlc;

use termion::input::TermRead;
use termion::event::Key;
use termion::raw::IntoRawMode;
use termion::color;

use chrono::prelude::*;

use std::{thread, time};
use std::io::{stdout, Write, stdin};
use std::sync::mpsc::{self, TryRecvError};

fn main() {
    let (tx, rx) = mpsc::channel();
    thread::spawn(move|| {
        let mut stdout = stdout().into_raw_mode().unwrap();
        let mut tick = true;
        let mut tick_count = 100;
        assert!(writeln!(stdout, "{}{}", termion::cursor::Goto(1,1), termion::clear::All).is_ok());
        loop {
            if tick_count <= 100 {
                tick_count += 1; 
            } else {
                tick_count = 0;
                for (i, l) in ascii_time(tick).iter().enumerate() {
                    assert!(writeln!(stdout, "{}{}{}{}", termion::clear::CurrentLine, termion::cursor::Goto(1, i as u16 + 1), color::Fg(color::LightGreen), l).is_ok());
                }
                tick = !tick;
            }

            let sec = time::Duration::from_millis(10);
            thread::sleep(sec);
            match rx.try_recv() {
                Ok(_) | Err(TryRecvError::Disconnected) => {
                    println!("{}Terminating clock thread.",  termion::cursor::Goto(1, 6));
                    break;
                }
                Err(TryRecvError::Empty) => {}
            }
        }
    });
    let stdin = stdin();
    
    for c in stdin.keys() {
        match c.unwrap() {
            Key::Ctrl('c') => {
                let _ = tx.send(());
                thread::sleep(time::Duration::from_millis(11));
                break;
            },
            _ => {}
        }
    }
    println!("{}{}{}Exiting main thread.\n", termion::clear::CurrentLine, termion::cursor::Goto(1, 7), color::Fg(color::Reset));
}

fn ascii_time(tick: bool) -> [String; 5] {
    let t = Local::now();
    let mut d: [String; 5] = Default::default();

    d = push_char(d, t.hour() / 10);
    d = push_char(d, t.hour() % 10);

    d = push_tick(d, tick);

    d = push_char(d, t.minute() / 10);
    d = push_char(d, t.minute() % 10);

    d = push_tick(d, tick);

    d = push_char(d, t.second() / 10);
    d = push_char(d, t.second() % 10);

    d
}

fn push_tick(mut str_arr: [String; 5], tick: bool) -> [String; 5] {
    for i in 0..5 {
        if i%2==1 && tick {
            str_arr[i].push_str(" *  ")
        } else {
            str_arr[i].push_str("    ")
        }
    }

    str_arr
}

fn push_char(mut str_arr: [String; 5], ascii_charr: u32) -> [String; 5] {
    for (i, r) in ASCII_CHARS[ascii_charr as usize].iter().enumerate() {
        for c in r {
            str_arr[i].push(*c);
        }
        str_arr[i].push(' ');
    }

    str_arr
}

static ASCII_CHARS: [[[char; 6]; 5]; 10] = [
    [
        ['0', '0', '0', '0', '0', '0'],
        ['0', '0', ' ', ' ', '0', '0'],
        ['0', '0', ' ', ' ', '0', '0'],
        ['0', '0', ' ', ' ', '0', '0'],
        ['0', '0', '0', '0', '0', '0'],
    ],
    [
        ['1', '1', '1', '1', ' ', ' '],
        [' ', ' ', '1', '1', ' ', ' '],
        [' ', ' ', '1', '1', ' ', ' '],
        [' ', ' ', '1', '1', ' ', ' '],
        ['1', '1', '1', '1', '1', '1'],
    ],
    [
        ['2', '2', '2', '2', '2', '2'],
        [' ', ' ', ' ', ' ', '2', '2'],
        ['2', '2', '2', '2', '2', '2'],
        ['2', '2', ' ', ' ', ' ', ' '],
        ['2', '2', '2', '2', '2', '2'],
    ],
    [
        ['3', '3', '3', '3', '3', '3'],
        [' ', ' ', ' ', ' ', '3', '3'],
        ['3', '3', '3', '3', '3', '3'],
        [' ', ' ', ' ', ' ', '3', '3'],
        ['3', '3', '3', '3', '3', '3'],
    ],
    [
        ['4', '4', ' ', ' ', '4', '4'],
        ['4', '4', ' ', ' ', '4', '4'],
        ['4', '4', '4', '4', '4', '4'],
        [' ', ' ', ' ', ' ', '4', '4'],
        [' ', ' ', ' ', ' ', '4', '4'],
    ],
    [
        ['5', '5', '5', '5', '5', '5'],
        ['5', '5', ' ', ' ', ' ', ' '],
        ['5', '5', '5', '5', '5', '5'],
        [' ', ' ', ' ', ' ', '5', '5'],
        ['5', '5', '5', '5', '5', '5'],
    ],
    [
        ['6', '6', '6', '6', '6', '6'],
        ['6', '6', ' ', ' ', ' ', ' '],
        ['6', '6', '6', '6', '6', '6'],
        ['6', '6', ' ', ' ', '6', '6'],
        ['6', '6', '6', '6', '6', '6'],
    ],
    [
        ['7', '7', '7', '7', '7', '7'],
        [' ', ' ', ' ', ' ', '7', '7'],
        [' ', ' ', ' ', ' ', '7', '7'],
        [' ', ' ', ' ', ' ', '7', '7'],
        [' ', ' ', ' ', ' ', '7', '7'],
    ],
    [
        ['8', '8', '8', '8', '8', '8'],
        ['8', '8', ' ', ' ', '8', '8'],
        ['8', '8', '8', '8', '8', '8'],
        ['8', '8', ' ', ' ', '8', '8'],
        ['8', '8', '8', '8', '8', '8'],
    ],
    [
        ['9', '9', '9', '9', '9', '9'],
        ['9', '9', ' ', ' ', '9', '9'],
        ['9', '9', '9', '9', '9', '9'],
        [' ', ' ', ' ', ' ', '9', '9'],
        [' ', ' ', ' ', ' ', '9', '9'],
    ],
];

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
